<!doctype html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  </head>
  <body style="font-family: sans-serif;">
    <div style="display: block; margin: auto; max-width: 600px; text-align:center" class="main">
      <h1 style="font-size: 18px; font-weight: bold; margin-top: 20px">Selamat saudara {{$name}} telah berhasil register</h1>
      <p style="background-color: yellow; font-weight: bold">{{$otp}}</p>
      <small>Batas aktif OTP hanya 5 menit</small>
    </div>
    <!-- Example of invalid for email html/css, will be detected by Mailtrap: -->
    <style>
      .main { background-color: white; }
      a:hover { border-left-width: 1em; min-height: 2em; }
    </style>
  </body>
</html>