<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthAPIControllers;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [AuthAPIControllers::class, 'Login']);
Route::post('/register', [AuthAPIControllers::class, 'Register']);

Route::middleware('api')->group(function () {

    Route::get('/logout', [AuthAPIControllers::class, 'Logout']);
    Route::get('/user', [AuthAPIControllers::class, 'getAuth']);
    Route::post('/update-user', [AuthAPIControllers::class, 'Update']);


});