<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Mail\UserRegisterMail;

class AuthAPIControllers extends Controller
{
    public function Login(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'Maaf, Akun tidak terdaftar'
            ]);
        }
        if(!$token = Auth::Attempt($request->only('email', 'password'))){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'E-mail / Password Salah ! !',
            ], 200);
        }

        // return response()->json(compact('token'));
        return response()->json([
            'response_code' => '00',
            'response_message' => 'user berhasil login',
            'data' => [
                'token' => $token,
                'user'  => auth()->user()
            ]
        ]);
    }

    public function Register(Request $request)
    {
        $cek = User::where('email', $request->email)->first();
        if ($cek) {
            return response()->json([
                'success' => false,
                'message' => 'Maaf e-Mail telah digunakan'
            ]);
        } else {
            $user = User::create([
                'name'     => $request->name,
                'email'    => $request->email,
                'password' => Hash::make($request->password)
            ]);

            $data['user'] = $user;

            Mail::to($user->email)->send(new UserRegisterMail($user));

            return response()->json([
                'success' => true,
                'message' => 'Akun berhasil dibuat',
                'data' => $data
            ]);
        }
    }
    
    public function Logout()
    {
        Auth::logout();
        return response()->json([
            'success' => true,
            'message' => 'Berhasil logout'
        ]);
    }

    public function Update(Request $request)
    {
        try {
            $user = User::where('email', $request->email)->update([
                'password' => bcrypt($request->password),
            ]);
            return response()->json([
                'status'  =>  true,
                'message' =>  'Berhasil memperbarui data',
                'data'    =>  $user
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  =>  false,
                'message' =>  'Gagal memperbarui data',
                'data'    =>  null
            ]);
        }
    }

    public function getAuth()
    {
        try {
            $auth = Auth::user();
            return response()->json([
                'status'  =>  true,
                'message' =>  'Berhasil mendapatkan data user',
                'data'    =>  $auth
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  =>  false,
                'message' =>  'Gagal mendapatkan data user',
                'data'    =>  null
            ]);
        }
    }
}
