<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;
use App\Models\User;
use Illuminate\Support\Str;



class OtpCodes extends Model
{
    use HasFactory, UsesUuid;

    protected $fillable = ['otp', 'valid_until', 'user_id'];
    // protected $primaryKey = 'id';

    protected static function boot()
    {
        parent::boot();

        static::creating( function($model){
            if( empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
