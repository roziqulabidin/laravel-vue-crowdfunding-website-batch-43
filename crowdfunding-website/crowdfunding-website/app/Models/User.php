<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    use HasFactory;

    protected $fillable = ['email','name','password','role_id', 'photo_profile'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()

    {
	    parent::boot();
	    static::creating( function($model){
		    if( empty($model->{$model->getKeyName()})){
			    $model->{$model->getKeyName()} = Str::uuid();
		    }
	    });
    }

    
}
