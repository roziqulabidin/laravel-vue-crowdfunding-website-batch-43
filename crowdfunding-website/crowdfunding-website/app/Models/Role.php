<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Role extends Model
{
    use HasFactory;

    protected $fillable = ['name'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
	    parent::boot();
	    static::creating( function($model){
		    if( empty($model->{$model->getKeyName()})){
			    $model->{$model->getKeyName()} = Str::uuid();
		    }
	    });
    }

    public function roles()
    {
        return $this->hasMany(User::class, 'role_id');
    }
}
